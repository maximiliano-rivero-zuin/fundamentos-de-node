const { argv } = require('./Config/yargs');
const { crearArchivo } = require('./js/creaTabla');

// Usando yargs
const base_yargs = argv.b;
const base = base_yargs ? base_yargs : 5;
const listar = argv.l;

crearArchivo(base, listar)
	.then(response => console.log(response))
	.catch(err => console.log(err));