const fs = require('fs');
const colors = require('colors');

const crearArchivo = (base = 5, listar) => {
	return new Promise((res, rej) => {
		//Hago la multiplicacion
		let table_str = '',
			table_colored = '',
			table_cyan = '';
		let result = 0;

		for (let i = 1; i <= 20; i++) {
			result = base*i;
			table_str += `${base} x ${i} = ${result}\n`;
			//colored
			table_cyan += `${colors.cyan(base)} x ${colors.cyan(i)} = ${colors.cyan(result)}\n`;
			table_colored += `${colors.magenta(base)} x ${colors.magenta(i)} = ${colors.green(result)}\n`;
		}
        
		if (listar) {
			//Imprimo la tabla por consola
			if (base == 10) {
				//argentina
				console.log(`----- ${colors.cyan('Tabla')} del ${colors.cyan(base)} -----`);
				console.log(table_cyan);
			} else{
				//magenta y verde
				console.log(`----- ${colors.rainbow('Tabla del')} ${colors.magenta(base)} -----`);
				console.log(table_colored); 
			}
		}
        
		//Escribo el archivo txt con la tabla de multiplicar correspondiente
		fs.writeFile(`./salida/tabla-${base}.txt`, table_str, (err) => {
			if (err) {
				rej(err => { throw err; });
			}else{
				res(`file tabla-${base}.txt created successfully`.green);
			}
		});  
	});
};

module.exports = {
	crearArchivo: crearArchivo
};