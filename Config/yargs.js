const { bgRed } = require('colors');

const argv = require('yargs')
    .option('b', {
        alias: 'base',
        type: 'number',
        demandOption: true,
        describe: 'Es la base de la tabla de multiplicar'
    }).option('l', {
        alias: 'listar',
        type: 'boolean',
        default: false,
        demandOption: false,
        describe: 'Muestra la tabla en consola'
    }).check((argv) => {
        // const filePaths = argv._
        if (isNaN(argv.b)) {
            throw new Error(bgRed("La base tiene que ser un numero."))
        } else if (argv.b<1 || argv.b>20) {
            throw new Error(bgRed("Debes colocar un valor de base entre 1 y 20"))  
        } else {
            return true // tell Yargs that the arguments passed the check
        }
        })
    .argv;


module.exports = {
    argv: argv
}